<?php
namespace Gam6itko\CommonBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class Gam6itkoCommonBundle
 * @package Gam6itko\CommonBundle
 */
class Gam6itkoCommonBundle extends Bundle
{
}
