<?php
namespace Gam6itko\CommonBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * Нужен для предотвращения попадания в базу идентичных сущностей. Используется в ReflecttorService
 * Class UniqueEntity
 * @Annotation
 * @Target("CLASS")
 * @package Bits\DeliveryBundle\Annotations
 */
class NotDuplicate extends Annotation
{
    /**
     * @var array - Список свойст Сущности, которые нужно проверить на уникальность.
     */
    public $fields = [];

    /**
     * @var array - Список свойст Сущности, которые не надо проверять на уникальность в базе
     */
    public $ignore = [];
}