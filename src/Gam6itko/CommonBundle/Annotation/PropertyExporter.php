<?php
namespace Gam6itko\CommonBundle\Annotation;

/**
 * Class EntityPropertyTree
 * @package Gam6itko\CommonBundle\Annotation
 *
 * @Annotation
 * @Target("PROPERTY")
 *
 */
class PropertyExporter
{
    /**
     * @var string - по этому лэйблу будут подбираться поля в нужных условиях
     */
    public $label = 'default';

    /**
     * @var bool
     */
    public $ignore = true;

    /**
     * @var bool - можно ли производить сортировку в таблицах по данному свойству
     */
    public $sortable = true;
}