<?php
namespace Gam6itko\CommonBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * Класс нужен чтобы записать данные из массива в объект.
 * @Annotation
 * @Target("PROPERTY")
 * @see ReflectorService
 * @see http://doctrine-orm.readthedocs.io/projects/doctrine-common/en/latest/reference/annotations.html
 */
class PropertyImporter extends Annotation
{
    const PROPERTY_DELIMITER = '.';
    /**
     * @var string|array - Название поля из источника. Если пустое, то берется название поля.
     * PROPERTY_DELIMITER разделяет вложенность массива (firstlevel.secondlevel эквивалентно [firstlevel => [secondlevel => value]]).
     * Если значение является массивом, то берется значение первого существующего параметра
     */
    public $theirField;

    /**
     * @var boolean - перезаписывать не пустое значение.
     */
    public $rewriteNotEmpty = true; //todo что если  $theirField будет состоять из массива ?

    /**
     * @var array|string - название функции данного класса которой передадут значение донора, а потом запишут в поле.
     */
    public $handler;

    /**
     * @var string - по этому лэйблу будут подбираться поля в нужных условиях
     */
    public $label = 'default';

    //todo import order
}
