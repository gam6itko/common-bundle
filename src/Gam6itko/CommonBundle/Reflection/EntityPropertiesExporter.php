<?php
namespace Gam6itko\CommonBundle\Reflection;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Gam6itko\CommonBundle\Annotation\PropertyExporter;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Строит дерево свойств объектов. Нужен для настроек колонок
 * Class EntityPropertiesTree
 * @package Gam6itko\CommonBundle\Reflection
 */
class EntityPropertiesExporter
{
    /**
     * @var AnnotationReader
     */
    protected $reader;

    protected $maxLevel = 2;

    protected $ignoredProperties = ['id'];

    /**
     * @var array типы ORM, которые нужно игнорировать
     */
    protected $ignoredTypes = ['json_array', 'datetime'];

    /** @var array страхуемся от повторного использования одних и тех же сущностей */
    protected $ignoreEntities = [];

    /**
     * @var array в данном массиве указаные возможные пути для разрешения имен классов
     */
    protected $classPath;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var string
     */
    protected $translationDomain = 'property_exporter';


    public function __construct($classPath, $label = 'default')
    {
        $this->classPath = trim($classPath, "\\");
        $this->reader = new AnnotationReader();
        $this->label = $label;
    }

    /**
     * @param array $ignoredTypes
     * @return $this
     */
    public function setIgnoredTypes(array $ignoredTypes)
    {
        $this->ignoredTypes = $ignoredTypes;
        return $this;
    }

    /**
     * @param TranslatorInterface $translator
     * @return $this
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
        return $this;
    }

    /**
     * @param $maxLevel
     * @return EntityPropertiesExporter
     */
    public function setMaxLevel($maxLevel)
    {
        if ($maxLevel < 0) {
            $maxLevel = 0;
        }
        if ($maxLevel > 10) {
            $maxLevel = 10;
        }
        $this->maxLevel = $maxLevel;
        return $this;
    }

    public function setIgnoredProperties(array $ignoredProperties)
    {
        $this->ignoredProperties = $ignoredProperties;
    }

    public function setIgnoredEntities(array $ignoreEntities)
    {
        foreach ($ignoreEntities as $ie) {
            if ($className = $this->resolveClassName($ie)) {
                $this->ignoreEntities[] = $className;
            }
        }
    }

    /**
     * @param string|object $entity
     * @return EntityPropertyNode
     * @throws \Exception
     */
    public function exportTree($entity)
    {
        $node = $this->prepareEntity($entity, 0, $refClass);
        if ($node) {
            $this->fill($node, $refClass);
        }
        return $node;
    }

    /**
     * @param $entity
     * @param int $level
     * @param null $refClass
     * @return EntityPropertyNode
     * @throws \Exception
     */
    protected function prepareEntity($entity, $level = 0, &$refClass = null)
    {
        if ($level > $this->maxLevel) {
            return null;
        }

        $className = $this->resolveClassName($entity);
        if ( ! $className) {
            throw new \Exception("full class name for `$entity`");
        }

        // игнорируем сущность, которую уже экспортнули todo может возникнуть проблема с уровнем сущности
        if (in_array($className, $this->ignoreEntities)) {
            return null;
        }

        $refClass = new ReflectionClass($className);
        $this->ignoreEntities[] = $refClass->getName();

        // check class is a Entity
        if ( ! $this->reader->getClassAnnotation($refClass, Entity::class)) {
            throw new \Exception('entity has not Entity annotation');
        }

        $ep = (new EntityPropertyNode(''))
            ->setTitle($this->trans($refClass->getShortName()))
            ->setEntity($refClass->getShortName())
            ->setLevel($level);
        return $ep;
    }

    protected function fill(EntityPropertyNode $node, ReflectionClass $refClass = null)
    {
        if ($refClass === null) {
            $refClass = new ReflectionClass($this->resolveClassName($node->getEntity()));
        }

        /** @var  ReflectionProperty $refProperty */
        foreach ($refClass->getProperties() as $refProperty) {
            if (in_array($refProperty->getName(), $this->ignoredProperties)) {
                continue;
            }

            $annotations = $this->reader->getPropertyAnnotations($refProperty);
            if ($this->needToIgnore($annotations)) {
                continue;
            }

            $propertyPath = implode('.', array_filter([$node->getPath(), $refProperty->getName()]));

            foreach ($annotations as $an) {
                $anClass = get_class($an);
                switch ($anClass) {
                    case Column::class:
                        /** @var Column $an */
                        if (in_array($an->type, $this->ignoredTypes)) {
                            break;
                        }
                        $ep = (new EntityPropertyNode($refProperty->getName()))
                            ->setTitle($this->trans($refProperty->getName()))
                            ->setPath($propertyPath)
                            ->setLevel($node->getLevel())
                            ->setType($an->type)
                            ->setNullable($an->nullable);
                        /** @var Column $an */
                        $node->addProperty($ep);
                        break;

                    case OneToOne::class:
                    case OneToMany::class:
                    case ManyToOne::class:
                    case ManyToMany::class:
                        $cNode = $this->prepareEntity($an->targetEntity, $node->getLevel() + 1);
                        if ($cNode) {
                            $cNode
                                ->setPath($propertyPath)
                                ->setRelation($this->getRelationConst($anClass))
                                ->setPropertyName($refProperty->getName());
                            $this->fill($cNode);
                            $node->addProperty($cNode);
                        }
                        break;
                }
            }
        }
    }

    /**
     * Позвращает номер отношений сущностей в базе
     * @param $anClassName
     * @return int|null
     */
    protected function getRelationConst($anClassName)
    {
        // логика такая OneToOne = 1к1, значит ставим 2 бита в единицы
        // соответственно, Many - бставим бит в 0
        switch ($anClassName) {
            case OneToOne::class:
                return 0b11;
            case OneToMany::class:
                return 0b10;
            case ManyToOne::class:
                return 0b01;
            case ManyToMany::class:
                return 0b00;
            default:
                return null;
        }
    }

    protected function resolveClassName($className)
    {
        $arr = [$className, $this->classPath . "\\" . $className];
        foreach ($arr as $a) {
            if (class_exists($a)) {
                return $a;
            }
        }
        return null;
    }

    /**
     * Проверяет поле на игнорирование. Отфильтровывает анотации PropertyExporter.
     * @param array $annotations
     * @return bool
     */
    private function needToIgnore(array &$annotations)
    {
        $result = false;
        $changed = false;
        $cnt = count($annotations);
        for ($i = 0; $i < $cnt; $i++) {
            $a = $annotations[$i];
            if ($a instanceof PropertyExporter) {
                /** @var PropertyExporter $a */
                if ($a->label === $this->label && $a->ignore) {
                    $result = true;
                }
                unset($annotations[$i]);
                $changed = true;
            }
        }
        if ($changed) {
            $annotations = array_values($annotations);
        }

        return $result;
    }

    private function trans($text)
    {
        if ($this->translator) {
            return $this->translator->trans($text, [], $this->translationDomain);
        }
        return $text;
    }
}