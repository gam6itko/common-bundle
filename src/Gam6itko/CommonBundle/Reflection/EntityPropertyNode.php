<?php
namespace Gam6itko\CommonBundle\Reflection;

use Doctrine\Common\Collections\ArrayCollection;

class EntityPropertyNode
{
    /**
     * @var string
     */
    protected $propertyName;

    /**
     * @var string путь к свойству в дереве сущностей, который формирует EntityPropertiesTree и использует PropertyAccessor
     */
    protected $path;

    /**
     * @var int
     */
    protected $level = 0;

    /**
     * @var string
     */
    protected $entity;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var integer
     */
    protected $relation = null;

    /**
     * @var null - Название поля для пользователя
     */
    protected $title = null;

    /**
     * @var null|ArrayCollection
     */
    protected $properties;

    protected $nullable = false;

    public function __construct($propertyName)
    {
        $this->propertyName = $propertyName;
        $this->properties = new ArrayCollection();
    }


    /**
     * @return string
     */
    public function getPropertyName()
    {
        return $this->propertyName;
    }

    /**
     * @param string $propertyName
     * @return $this
     */
    public function setPropertyName($propertyName)
    {
        $this->propertyName = $propertyName;
        return $this;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param int $level
     * @return $this
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     * @return $this
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return integer
     */
    public function getRelation()
    {
        return $this->relation;
    }

    /**
     * @param integer $relation
     * @return $this
     */
    public function setRelation($relation)
    {
        $this->relation = $relation;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param $propertyName
     * @return EntityPropertyNode
     */
    public function findProperty($propertyName)
    {
        $result = null;
        foreach ($this->properties as $p) {
            if ($p->getPropertyName() == $propertyName) {
                return $p;
            }
        }
    }

    /**
     * @param EntityPropertyNode $pn
     */
    public function addProperty(EntityPropertyNode $pn)
    {
        if (!$pn->getPath()) {
            $propertyPath = implode('.', array_filter([$this->getPath(), $pn->getPropertyName()]));
            $pn->setPath($propertyPath);
        }
        $this->properties->add($pn);
    }

    /**
     * @return array
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isNullable()
    {
        return $this->nullable;
    }

    /**
     * @param boolean $nullable
     * @return $this
     */
    public function setNullable($nullable)
    {
        $this->nullable = $nullable;
        return $this;
    }

    /**
     * @return null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }



}