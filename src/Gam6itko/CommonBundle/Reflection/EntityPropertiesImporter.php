<?php
namespace Gam6itko\CommonBundle\Reflection;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Bridge\Doctrine\Form\Type\DoctrineType;

class EntityPropertiesImporter
{
    /**
     * @var array в данном массиве указаные возможные пути для разрешения имен классов
     */
    protected $classPath;


    public function __construct($classPath)
    {
        $this->classPath = trim($classPath, "\\");
        $this->reader = new AnnotationReader();
    }

    /**
     * создает древовидный многомерный массив, расшинеррый array_combine
     * @param array $keysTree - одномерный массив ключей
     * @param array $valuesTree - одномерный массив значений
     * @param bool $nullOnEmpty - если массив содержит пустые значние, то возвращаем тгдд
     * @return array
     */
    public function arrayCombineTree(array $keysTree, array $valuesTree = [], $nullOnEmpty = false)
    {
        $result = [];
        foreach ($keysTree as $i => $path) {
            if ( ! $path) {
                continue;
            }
            $tmp = explode('.', $path);
            $propName = array_shift($tmp);
            if (count($tmp) > 0) {
                $np = implode('.', $tmp);
                $result[$propName]['paths'][] = $np;
                $result[$propName]['values'][] = array_key_exists($i, $valuesTree) ? $valuesTree[$i] : null;;
            } else {
                $result[$propName] = array_key_exists($i, $valuesTree) ? $valuesTree[$i] : null;
            }
        }
        unset($keysTree, $valuesTree);

        foreach ($result as $propertyName => $value) {
            if (is_array($value)) {
                $result[$propertyName] = $this->arrayCombineTree($value['paths'], $value['values'], $nullOnEmpty);
            }
        }

        if ($nullOnEmpty && ! array_filter($result)) {
            return null;
        }

        return $result;
    }

    /**
     * Оставляет информацию только по тем свойствам, пути к которым находятся в $leavePaths
     * @param EntityPropertyNode $node
     * @param array $tree
     * @return array
     * @internal param array $leavePaths - оставить только эти значения
     */
    public function filterUnnecessary(EntityPropertyNode $node, array $tree = null)
    {
        if ($tree === null) {
            $tree = [];
        }
        /** @var EntityPropertyNode $p */
        foreach ($node->getProperties() as $p) {
            if ( ! array_key_exists($p->getPropertyName(), $tree)) {
                $node->getProperties()->removeElement($p);
                continue;
            } else if (is_array($tree[$p->getPropertyName()])) {
                $this->filterUnnecessary($p, $tree[$p->getPropertyName()]);
            }
        }
    }

    /**
     *
     * @param EntityPropertyNode $epNode - колонки сущности
     * @param array $propertiesValueTree - древовидная структура значений
     * @param \Closure $fnCreate - функция создания сущности,
     * @return mixed
     */
    public function importTree(EntityPropertyNode $epNode, array $propertiesValueTree, \Closure $fnCreate)
    {
        $myProperties = [];
        foreach ($propertiesValueTree as $k => $v) {
            $newValue = $v;
            if (is_array($v)) {
                if ($this->isAssoc($v)) {
                    $newValue = $this->extract($epNode, $k, $v, $fnCreate, true);
//                    if ($epNode->getRelation() === 0b10) {
//                        $newValue = [$newValue];
//                    }
                } else {
                    $newValue = [];
                    foreach ($v as $d) {
                        $newValue[] = $this->extract($epNode, $k, $d, $fnCreate);
                    }
                }
            }
            $myProperties[$k] = $newValue;
        }

        $className = $this->resolveClassName($epNode->getEntity());

        return $fnCreate($className, $myProperties, $epNode);
    }

    protected function extract(EntityPropertyNode $epNode, $k, $v, $fnCreate, $wrapOneToMany = false)
    {
        $result = null;
        /** @var EntityPropertyNode $epn */
        foreach ($epNode->getProperties() as $epn) {
            if ($epn->getPropertyName() === $k) {
                $result = $this->importTree($epn, $v, $fnCreate);
                if ($wrapOneToMany && $epn->getRelation() === 0b10) {
                    $result = [$result];
                }
                break;
            }
        }

        return $result;
    }

    protected function resolveClassName($className)
    {
        $arr = [$className, $this->classPath . "\\" . $className];
        foreach ($arr as $a) {
            if (class_exists($a)) {
                return $a;
            }
        }

        return null;
    }

    private function isAssoc(array $arr)
    {
        if ([] === $arr) {
            return false;
        }

        return array_keys($arr) !== range(0, count($arr) - 1);
    }
}