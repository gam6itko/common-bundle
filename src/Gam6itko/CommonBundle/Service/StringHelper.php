<?php
namespace Gam6itko\CommonBundle\Service;

class StringHelper
{
    /**
     * Удаляет символы, которые недопустимы в именах вайлов
     * @param $filename
     * @return mixed
     */
    public function sanitizeFilename($filename)
    {
        $filename = str_replace(' ', '_', $filename);
        return preg_replace("/[^a-z0-9\._-]+/i", '', $filename);
    }

    /**
     * @param $string
     * @return mixed
     * @see https://gist.github.com/vindia/1476814
     */
    public function transliterate($string)
    {
        return transliterator_transliterate('Russian-Latin/BGN', $string);
    }
}