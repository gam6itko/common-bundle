<?php
namespace Gam6itko\CommonBundle\Service;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Gam6itko\CommonBundle\Annotation\NotDuplicate;
use Gam6itko\CommonBundle\Annotation\PropertyImporter;
use Monolog\Logger;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;


/**
 * Бывает так, что надо установить несколько параметров объекта из массива. вызывать Кучи setSomething, уменьшает читаемость кода.
 * Упор делается на DoctrineAnnotation, проставил нужные аннотации - данный сервис сам все проверил/заполнил
 * Class ReflectorService
 * @package Bits\DeliveryBundle\Service
 */
class ReflectorService
{
    use ContainerAwareTrait;

    /**
     * ReflectorService constructor.
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ContainerInterface $serviceContainer)
    {
        $this->container = $serviceContainer;
    }

    /**
     * Заполняет Сущность данными из массива на основании аннотации полей PropertyImporter
     * @param $entity
     * @param array $fillData
     * @param $label - из PropertyImporter
     */
    public function importProperties($entity, array $fillData, $label = 'default')
    {
        if (empty($fillData)) {
            return;
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $reader = new AnnotationReader();
        $refClass = new ReflectionClass($entity);

        // перебираем свойства класса и ищем Аннотацию для адаптации полей
        foreach ($refClass->getProperties() as $property) {
            $annArr = $reader->getPropertyAnnotations($property);
            if (empty($annArr)) {
                continue;
            }

            /** @var PropertyImporter|null $annotation */
            $annotation = null;
            // ищем анатацию с указанным лейблом
            foreach ($annArr as $a) {
                if (is_a($a, PropertyImporter::class) && $a->label == $label) {
                    $annotation = $a;
                    break;
                }
            }

            if ($annotation === null) {
                continue;
            }

            // получаем название поля в массиве, которое нужно записать в сущность
            $tfn = $annotation->theirField ?: $property->getName();
            if (!is_array($tfn)) {
                $tfn = [$tfn];
            }
            foreach ($tfn as $t) {
                $theirFieldName = $this->prepareForPa($t);
                if (!$propertyAccessor->isReadable($fillData, $theirFieldName)) {
                    $this->getLogger()->debug('fillEntity. fillData not found or null', [$theirFieldName]);
                    continue;
                }

                // название параметра может быть разделено точками (.)
                $newValue = $propertyAccessor->getValue($fillData, $theirFieldName);
                $oldValue = $propertyAccessor->getValue($entity, $property->getName());

                if (!empty($annotation->handler)) {
                    if (!class_exists($annotation->handler['class'])) {
                        $this->getLogger()->debug("PropertyImporter.handler.class not found", ['class' => get_class($entity), 'fieldname' => $theirFieldName, 'handler' => $annotation->handler]);
                        continue;
                    }
                    if (!method_exists($annotation->handler['class'], $annotation->handler['method'])) {
                        $this->getLogger()->debug("PropertyImporter.handler.method not found", ['class' => get_class($entity), 'fieldname' => $theirFieldName, 'handler' => $annotation->handler]);
                        continue;
                    }
                    call_user_func([$annotation->handler['class'], $annotation->handler['method']], $entity, $newValue, $theirFieldName);
                } else if ($newValue) {
                    if (empty($oldValue) || $annotation->rewriteNotEmpty) {
                        $propertyAccessor->setValue($entity, $property->getName(), $newValue);
                    }
                    break;
                }
            }
        }
    }

    /**
     * Проверяет есть ли в базе уже запись со указанными значениями полей, использует NotDuplicate Annotation.
     * Если свойство объекта пустое, то оно игнорируется для проверки
     *
     * @param $entity
     * @return null|mixed - Дубликат в базе, если имеется
     */
    public function checkDuplication($entity)
    {
        try {
            $reader = new AnnotationReader();
            $refClass = new ReflectionClass($entity);

            /** @var NotDuplicate $anNotDuplicate */
            $anNotDuplicate = $reader->getClassAnnotation($refClass, NotDuplicate::class);
            /** @var Entity $anIntity */
            $anIntity = $reader->getClassAnnotation($refClass, Entity::class);

            if (!$anNotDuplicate || !$anIntity) {
                return null;
            }
            unset($anIntity);

            $qb = $this->getEntityManager()->createQueryBuilder()
                ->select('e')
                ->from($refClass->getName(), 'e')
                ->setMaxResults(1);

            $fields = $anNotDuplicate->fields;
            // если не указаны никакие поля для проверки, то проверяем все с аннотацией Column
            if (empty($fields)) {
                /** @var \ReflectionProperty $refProp */
                foreach ($refClass->getProperties() as $refProp) {
                    foreach ($reader->getPropertyAnnotations($refProp) as $anProp) {
                        if (is_a($anProp, Column::class) || is_a($anProp, ManyToOne::class) || is_a($anProp, OneToOne::class)) {
                            $fields[] = $refProp->getName();
                            break;
                        }
                    }
                }
            } else {
                $fields = array_filter($fields, function ($fieldName) use ($refClass) {
                    if (!$refClass->hasProperty($fieldName)) {
                        $this->getLogger()->warning("Property `$fieldName` does not exists in `{$refClass->getName()}`. Check NotDuplicate class annotation");
                    }
                    return $refClass->hasProperty($fieldName);
                });
            }

            if ($anNotDuplicate->ignore) {
                $fields = array_diff($fields, $anNotDuplicate->ignore);
            }

            if (empty($fields)) {
                // у нас нет полей по которым надо искать уникальную запись. мы так не работаем!
                return null;
            }

            $hasProperties = false;
            foreach ($fields as $fieldName) {
                $refProp = $refClass->getProperty($fieldName);
                $refProp->setAccessible(true);
                /** @var  $fVal - значение поля в базе */
                $fVal = $refProp->getValue($entity);
                if (empty($fVal)) { //todo возможно нужна настройка
                    // не будем искать по пустым полям
                    continue;
                }
                if (is_object($fVal) && method_exists($fVal, 'getId') && $fVal->getId() == null) {
//                    $this->getLogger()->warning("Value for roperty `$fieldName` does not have Id. You should persist it first");
                    continue;
                }
                $qb->andWhere("e.$fieldName = :{$fieldName}_token")
                    ->setParameter(":{$fieldName}_token", $fVal);
            }

            if ($qb->getParameters()->count() == 0) {
                // нет параметров для поиска
                return null;
            }

            return $qb->getQuery()->getOneOrNullResult();
        } catch (\Exception $exc) {
            $this->getLogger()->warning($exc);
            return null;
        }
    }


    /**
     * Преобразует путь из firstlevel.secondlevel к [firstlevel][secondlevel]
     * @see https://symfony.com/doc/current/components/property_access/introduction.html#reading-from-arrays
     * @param $theirFieldName
     * @return mixed
     */
    protected function prepareForPa($theirFieldName)
    {
        if (strpos($theirFieldName, PropertyImporter::PROPERTY_DELIMITER) === false) {
            return "[$theirFieldName]";
        }

        $pArr = explode(PropertyImporter::PROPERTY_DELIMITER, $theirFieldName);
        return array_reduce($pArr, function ($carry, $item) {
            return $carry . "[$item]";
        });
    }

    /**
     * @return Logger
     */
    protected function getLogger()
    {
        // сервис настроен при помощи конфигурации логера
        return $this->container->get('logger');
    }

    /**
     * @return EntityManager
     */
    protected final function getEntityManager()
    {
        return $this->container->get('doctrine')->getManager();
    }

}