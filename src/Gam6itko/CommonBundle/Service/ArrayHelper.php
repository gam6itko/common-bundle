<?php
namespace Gam6itko\CommonBundle\Service;

use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

class ArrayHelper
{
    /**
     * @var CamelCaseToSnakeCaseNameConverter
     */
    protected $camelizer = null;

    /**
     * Преобразует все ключи массива в camelCase
     * @param array $array
     * @return array
     */
    public function arrayKeysCamelize(array $array)
    {
        if (!$this->camelizer) {
            /** @var CamelCaseToSnakeCaseNameConverter $c */
            $this->camelizer = new CamelCaseToSnakeCaseNameConverter();
        }

        $result = [];
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                $v = $this->arrayKeysCamelize($v);
            }
            $result[$this->camelizer->denormalize($k)] = $v;
        }

        return $result;
    }

    public function walkRecursiveRemove(array $array, callable $callback)
    {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                $array[$k] = $this->walkRecursiveRemove($v, $callback);
            } else {
                if ($callback($v, $k)) {
                    unset($array[$k]);
                }
            }
        }

        return $array;
    }

}